import { render } from 'react-dom'
import App from './AppMerge/containers/App'

render(<App />, document.getElementById('root'))
